from django.shortcuts import render

# Create your views here.
def indexForLoadBalancer(request):
    host = request.get_host()
    port = request.get_port()
    content = {
        'host':host,
        'port':port,
    }
    return render(request, 'index.html', content)
