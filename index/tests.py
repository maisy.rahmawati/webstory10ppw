from django.test import TestCase, Client
from .views import indexForLoadBalancer

# Create your tests here.
class UnitTest(TestCase):
    #Test untuk cek url index
    def test_url_address_index_is_exist(self):
        response = Client().get('/index/')
        self.assertEqual(response.status_code, 200)

    #Test untuk cek alamat url index tidak ada
    def test_url_address_index_is_not_exist(self):
        response = Client().get('/')
        self.assertNotEqual(response.status_code, 200)

    #Test untuk cek fungsi indexForLoadBalancer telah memiliki isi
    def test_login_is_written(self):
        self.assertIsNotNone(indexForLoadBalancer)

    #Test untuk cek template html telah digunakan pada halaman index
    def test_template_html_login_is_exist(self):
        response = Client().get('/index/')
        self.assertTemplateUsed(response, 'index.html')
