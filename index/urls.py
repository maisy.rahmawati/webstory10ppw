from django.conf.urls import url
from . import views
from django.urls import path

app_name = 'index'
urlpatterns = [
        path('', views.indexForLoadBalancer, name = 'indexForLoadBalancer'),
    ]
